//
//  DetailRepoView.swift
//  GenGitTest
//
//  Created by admin on 10/18/18.
//  Copyright © 2018 VikSolo. All rights reserved.
//

import UIKit
import WebKit

class DetailRepoView: UIViewController, DetailViewProtocol {

    public var repositoryURL: URL!
    
    @IBOutlet fileprivate var detailWebView: WKWebView!
    @IBOutlet fileprivate var loadingHUD: UIActivityIndicatorView!
    
    var presenter:DetailPresenterProtocol?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        detailWebView.navigationDelegate = self
        detailWebView.load(URLRequest(url: repositoryURL))
    }
    
    func updateView() {
    }
    
    @IBAction func closeDetail(_ sender: Any) {
        presenter?.wireframe?.backToSearch(from: self)
    }
}

extension DetailRepoView: WKNavigationDelegate {
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        loadingHUD.stopAnimating()
    }
}
