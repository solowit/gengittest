//
//  SearchViewController.swift
//  GenGitTest
//
//  Created by admin on 10/18/18.
//  Copyright © 2018 VikSolo. All rights reserved.
//

import UIKit

class SearchViewController: UIViewController {

    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var searchResultTable: UITableView!
    
    @IBOutlet weak var pauseButton: UIButton!
    @IBOutlet weak var startButton: UIButton!
    @IBOutlet weak var stopButton: UIButton!
    
    fileprivate var editMode:Bool = false {
        didSet {
            searchResultTable.isEditing = editMode
            updateResultTable()
        }
    }
    
    var presenter:SearchPresenterProtocol?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter?.setupView()
        enableButtons(false)
        
        let editButton = UIBarButtonItem(title: "Edit", style: .done, target: self, action: #selector(changeEditMode))
        self.navigationItem.rightBarButtonItem  = editButton
    }
    
    func enableButtons(_ enable: Bool) {
        pauseButton.isEnabled = enable
        stopButton.isEnabled = enable
    }
    
    func updateStateButton() {
        let isPaused = (presenter?.interactor?.isPause ?? false && presenter?.interactor?.isSearching ?? false)
        if isPaused {
            pauseButton.setTitle("resume".uppercased(), for: .normal)
        } else {
            pauseButton.setTitle("pause".uppercased(), for: .normal)
        }
    }
    
    @objc func changeEditMode() {
        editMode = !editMode
    }
    
    //MARK: - IBActions -
    
    @IBAction func pauseSearch(_ sender: Any) {
        let isPaused = presenter?.interactor?.isPause ?? false
        if !isPaused {
            presenter?.interactor?.pauseSearch()
        } else {
            presenter?.interactor?.startSearch(for: searchBar.text!)
        }
        updateStateButton()
    }
    
    @IBAction func startSearch(_ sender: Any) {
        presenter?.interactor?.startSearch(for: searchBar.text!)
        updateResultTable()
        enableButtons(true)
    }
    
    @IBAction func stopSearch(_ sender: UIButton) {
        enableButtons(false)
        presenter?.interactor?.stopSearch()
        updateStateButton()
    }
    
    @IBAction func gitLogin(_ sender: UIButton) {
        presenter?.wireframe?.navigateToLogin(from: self)
    }
}

extension SearchViewController: SearchViewProtocol {
    
    func showAlert(text: String) {
        showAlert(title: text)
    }
    
    func updateResultTable() {
        Thread.runInMain {[unowned self] in
            self.searchResultTable.reloadData()
        }
    }
}

extension SearchViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, editActionsForRowAt: IndexPath) -> [UITableViewRowAction]? {
        let remove = UITableViewRowAction(style: .normal, title: "Delete") {[weak self] action, index in
            let movedObject = self?.presenter?.interactor?.fetchResultController.object(at: index)
            if movedObject != nil {
                self?.presenter?.interactor?.removeObject(repository: movedObject!)
            }
        }
        return [remove]
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, moveRowAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
        let _ = presenter?.interactor?.fetchResultController.object(at: sourceIndexPath)
        presenter?.interactor?.changeObjectsPosition(from: sourceIndexPath, to: destinationIndexPath)
    }
    
    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCell.EditingStyle {
        return .delete
    }
    
    func tableView(_ tableView: UITableView, shouldIndentWhileEditingRowAt indexPath: IndexPath) -> Bool {
        return false
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let repository = presenter?.interactor?.fetchResultController.object(at: indexPath)
        presenter?.showRepoDetail(for: repository!, from: self)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let objects = presenter?.interactor?.fetchResultController.fetchedObjects ?? [Repository]()
        return objects.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let repository = presenter?.interactor?.fetchResultController.object(at: indexPath)
        let cell = UITableViewCell()
        cell.textLabel?.text = "\(indexPath.row). " + repository!.name!
        cell.selectionStyle = .none
        
        presenter?.interactor?.loadNewPageResult(for: searchBar.text!, for: indexPath)
        
        return cell
    }
    
}

extension SearchViewController: UISearchBarDelegate {
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        startSearch(searchBar)
        view.endEditing(true)
    }
}

