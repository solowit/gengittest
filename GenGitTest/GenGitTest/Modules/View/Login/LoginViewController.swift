//
//  LoginViewController.swift
//  GenGitTest
//
//  Created by admin on 10/19/18.
//  Copyright © 2018 VikSolo. All rights reserved.
//

import UIKit
import WebKit

class LoginViewController: UIViewController, LoginViewProtocol {
    
    @IBOutlet fileprivate var webView: WKWebView!
   
    var presenter:LoginPresenterProtocol?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        webView.navigationDelegate = self
        webView.load(URLRequest(url: URL(string: gitLoginURL)!))
    }
    
    func updateView() {
    }
}

extension LoginViewController: WKNavigationDelegate {
    
    public func webView(_ webView: WKWebView, decidePolicyFor navigationResponse: WKNavigationResponse, decisionHandler: @escaping (WKNavigationResponsePolicy) -> Void) {
        decisionHandler(.allow)
        
        if let url = navigationResponse.response.url {
            if let code = url.query?.components(separatedBy:"code=").last {
                presenter?.interactor?.getAccessToken(for: code) {[weak self] in
                    self?.presenter?.wireframe?.backToSearch(from: self!)
                }
            }
        }
    }
}

