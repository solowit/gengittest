//
//  SearchInteractor.swift
//  GenGitTest
//
//  Created by admin on 10/18/18.
//  Copyright © 2018 VikSolo. All rights reserved.
//

import Foundation
import CoreData

class SearchInteractor: NSObject, SearchInteractorProtocol {
    
    var isSearching = false
    var isPause = false
    
    var currentPage = 1
    var loadAllPages = false
    
    var lastSearchName = CoreDataManager.shared.getLastNameSearched() ?? "" {
        didSet {
            try? fetchResultController.performFetch()
        }
    }
    
    public var fetchResultController: NSFetchedResultsController<Repository> {
        
        let context = CoreDataManager.shared.persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<Repository>(entityName: String(describing: Repository.self))
        let sortStarDescriptor = NSSortDescriptor(key: "stars", ascending: false)
        let sortPositionDescriptor = NSSortDescriptor(key: "position", ascending: true)
        fetchRequest.sortDescriptors = [sortPositionDescriptor, sortStarDescriptor]
        
        let predicate = NSPredicate(format: "name contains[c] %@", lastSearchName)
        fetchRequest.predicate = predicate
        
        let fetchController = NSFetchedResultsController<Repository>(fetchRequest: fetchRequest, managedObjectContext: context, sectionNameKeyPath: nil, cacheName: nil)
        fetchController.delegate = self
        
        try? fetchController.performFetch()
        
        return fetchController
    }
    
    weak var presenter: SearchIntractorOutProtocol?
    
    func getResentSearchResult() {
        try? fetchResultController.performFetch()
    }
    
    func setViewedResult(for name: String) {
        CoreDataManager.shared.changeViewedRepository(for:name)
    }
    
    func startSearch(for name: String) {
        if (APIManager.instance.accessToken.isEmpty) {
            presenter?.getErrorForOperation(error: APIError.notLoginned)
            return
        }
        
        currentPage = 1
        repeatSearch()
        lastSearchName = name
        getSearchResult(for: name)
        CoreDataManager.shared.saveLastNameSearch(name:name)
    }
    
    func repeatSearch() {
        isSearching = true
        isPause = false
        loadAllPages = false
    }
    
    func pauseSearch() {
        isSearching = true
        isPause = true
        APIManager.instance.pauseAllDataTask()
    }
    
    func stopSearch() {
        isSearching = false
        isPause = true
        APIManager.instance.stopAllDataTask()
    }
    
    func resumeSearch() {
        isSearching = true
        isPause = false
        APIManager.instance.resumeAllDataTask()
    }
    
    
    func loadNewPageResult(for name: String, for indexPath:IndexPath) {
        
        if (APIManager.instance.accessToken.isEmpty) {
            return
        }
        
        if let count = fetchResultController.fetchedObjects?.count, count > 15, indexPath.row - 5 < count, !isSearching, !isPause, APIManager.instance.canLoadMore() {
            repeatSearch()
            currentPage += 1
            getSearchResult(for: name)
        }
    }
    
    func getSearchResult(for name: String = "") {
        APIManager.instance.getRepository(for: name, page: currentPage, success: { [weak self] in

            self?.isSearching = false
            self?.isPause = false
            
            self?.presenter?.gitReposDidFetch()
        }) {[weak self] errorMessage in
            
            self?.isSearching = false
            self?.isPause = false
            
            if errorMessage == .empty {
//                print(errorMessage)
//                self?.loadAllPages = true
            }
        }
    }
    
    func changeObjectsPosition(from sourceIndexPath:IndexPath, to destinationIndexPath:IndexPath) {
        var objects = fetchResultController.fetchedObjects!
        
        fetchResultController.delegate = nil
        
        let object = objects[sourceIndexPath.row]
        objects.remove(at:sourceIndexPath.row)
        objects.insert(object, at: destinationIndexPath.row)
        
        var i = 0
        for object in objects {
            object.position = Int32(i)
            i += 1
        }
        
        CoreDataManager.shared.saveContext()
        
        fetchResultController.delegate = self
    }
    
    func removeObject(repository: Repository) {
        CoreDataManager.shared.removeObject(repository: repository)
        presenter?.gitReposDidFetch()
    }
}

extension SearchInteractor: NSFetchedResultsControllerDelegate {
    
    public func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        presenter?.gitReposDidFetch()
    }
    
    public func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange sectionInfo: NSFetchedResultsSectionInfo, atSectionIndex sectionIndex: Int, for type: NSFetchedResultsChangeType) {
        presenter?.gitReposDidFetch()
    }
    
    public func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        presenter?.gitReposDidFetch()
    }
    
    public func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        presenter?.gitReposDidFetch()
    }
    
}
