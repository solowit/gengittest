//
//  LoginInteractor.swift
//  GenGitTest
//
//  Created by admin on 10/22/18.
//  Copyright © 2018 VikSolo. All rights reserved.
//

import Foundation

class LoginInteractor: LoginInteractorProtocol {
    var presenter: LoginPresenterProtocol?
    
    func getAccessToken(for code: String, complication: @escaping successEmptyComplication) {
        print("LoginInteractor")
        APIManager.instance.getAccessToken(code: code) {
            complication()
        }
    }
}
