//
//  GitRepo.swift
//  GenGitTest
//
//  Created by admin on 10/18/18.
//  Copyright © 2018 VikSolo. All rights reserved.
//

import Foundation

struct GitRepo: Codable {
    
    var id: Int
    var name: String
    var htmlURL: String
    var starsCount: Int
    
    private enum CodingKeys : String, CodingKey {
        case id, htmlURL = "html_url", name, starsCount = "stargazers_count"
    }
}
