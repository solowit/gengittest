//
//  SearchProtocols.swift
//  GenGitTest
//
//  Created by admin on 10/18/18.
//  Copyright © 2018 VikSolo. All rights reserved.
//

import Foundation
import UIKit
import CoreData

protocol SearchViewProtocol: class {
    
    var presenter: SearchPresenterProtocol? {get set}
    
    func updateResultTable()
    func showAlert(text: String)
}

protocol SearchPresenterProtocol: class {
    
    var interactor: SearchInteractorProtocol? {get set}
    var view: SearchViewProtocol? {get set}
    var wireframe: SearchRouterProtocol? {get set}
    
    func setupView()
    
    func showRepoDetail(for repo: Repository, from view: UIViewController)
}

protocol SearchInteractorProtocol: class {
    
    var fetchResultController: NSFetchedResultsController<Repository> {get}
    
    var isSearching: Bool {get set}
    var isPause: Bool {get set}
    
    var presenter: SearchIntractorOutProtocol? {get set}
    
    func startSearch(for name: String)
    func pauseSearch()
    func stopSearch()
    func resumeSearch()
    
    func loadNewPageResult(for name: String, for indexPath: IndexPath)
    
    func changeObjectsPosition(from sourceIndexPath:IndexPath, to destinationIndexPath:IndexPath)
    func removeObject(repository: Repository)
    func getResentSearchResult()
    func setViewedResult(for name: String)
}

protocol SearchRouterProtocol: class {
    
    static func createSearchModule() -> SearchViewController
    
    func navigateToLogin(from view: UIViewController)
    func navigateToDetail(with repoDetail:Repository, for view: UIViewController)
}

protocol SearchIntractorOutProtocol: class {
    func gitReposDidFetch()
    func getErrorForOperation(error:APIError)
}
