//
//  LoginProtocols.swift
//  GenGitTest
//
//  Created by admin on 10/22/18.
//  Copyright © 2018 VikSolo. All rights reserved.
//

import Foundation

protocol LoginViewProtocol: class {
    var presenter: LoginPresenterProtocol? {get set}
    
    func updateView()
}

protocol LoginPresenterProtocol {
    
    var interactor: LoginInteractorProtocol? {get set}
    var view: LoginViewProtocol? {get set}
    var wireframe: LoginRouterProtocol? {get set}
    
    func setupView()
}

protocol LoginInteractorProtocol {
    func getAccessToken(for code: String, complication: @escaping successEmptyComplication)
}

protocol LoginRouterProtocol {
    static func createLoginModule(loginView: LoginViewController)
    func backToSearch(from view: LoginViewController)
}
