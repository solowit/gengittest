//
//  DetailProtocols.swift
//  GenGitTest
//
//  Created by admin on 10/18/18.
//  Copyright © 2018 VikSolo. All rights reserved.
//

import Foundation

protocol DetailViewProtocol: class {
    var presenter: DetailPresenterProtocol? {get set}
    
    func updateView()
}

protocol DetailPresenterProtocol {
    
    var interactor: DetailInteractorProtocol? {get set}
    var view: DetailViewProtocol? {get set}
    var wireframe: DetailRouterProtocol? {get set}
    
    func setupView()
}

protocol DetailInteractorProtocol {
}

protocol DetailRouterProtocol {
    static func createDetailRepoModule(detailView: DetailRepoView, for repoDetail: Repository)
    func backToSearch(from view: DetailRepoView)
}
