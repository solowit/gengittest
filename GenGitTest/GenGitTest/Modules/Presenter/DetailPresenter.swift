//
//  DetailPresenter.swift
//  GenGitTest
//
//  Created by admin on 10/22/18.
//  Copyright © 2018 VikSolo. All rights reserved.
//

import Foundation

class DetailPresenter: DetailPresenterProtocol {
    
    var interactor: DetailInteractorProtocol?
    var view: DetailViewProtocol?
    var wireframe: DetailRouterProtocol?
    
    func setupView() {
    }
    
}
