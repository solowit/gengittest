//
//  LoginPresenter.swift
//  GenGitTest
//
//  Created by admin on 10/22/18.
//  Copyright © 2018 VikSolo. All rights reserved.
//

import Foundation

class LoginPresenter: LoginPresenterProtocol {
    
    var interactor: LoginInteractorProtocol?
    
    var view: LoginViewProtocol?
    
    var wireframe: LoginRouterProtocol?
    
    func setupView() {
    }
    
}
