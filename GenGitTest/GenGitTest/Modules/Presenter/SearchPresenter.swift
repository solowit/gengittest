//
//  SearchPresenter.swift
//  GenGitTest
//
//  Created by admin on 10/18/18.
//  Copyright © 2018 VikSolo. All rights reserved.
//

import Foundation
import UIKit

class SearchPresenter: SearchPresenterProtocol {
    
    var isSearching = true
    var isPause = false
    
    var interactor: SearchInteractorProtocol?
    var view: SearchViewProtocol?
    var wireframe: SearchRouterProtocol?
    
    func setupView() {
        //show result from last search
        interactor?.getResentSearchResult()
    }
    
    func showRepoDetail(for repo: Repository, from view: UIViewController) {
        interactor?.setViewedResult(for: repo.name!)
        wireframe?.navigateToDetail(with: repo, for: view)
    }
}

extension SearchPresenter: SearchIntractorOutProtocol {
    func getErrorForOperation(error: APIError) {
        view?.showAlert(text: error.errorDescription())
    }
    
    func gitReposDidFetch() {
        view?.updateResultTable()
    }
}
