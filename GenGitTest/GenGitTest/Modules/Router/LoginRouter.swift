//
//  LoginRouter.swift
//  GenGitTest
//
//  Created by admin on 10/22/18.
//  Copyright © 2018 VikSolo. All rights reserved.
//

import Foundation

class LoginRouter: LoginRouterProtocol {
    
    static func createLoginModule(loginView: LoginViewController) {
        
        let presenter: LoginPresenterProtocol = LoginPresenter()
        loginView.presenter = presenter
        loginView.presenter?.wireframe = LoginRouter()
        loginView.presenter?.view = loginView
        loginView.presenter?.interactor = LoginInteractor()
    }
    
    func backToSearch(from view: LoginViewController) {
        Thread.runInMain {
            view.showAlert(title: "Login succeed", action: {
                view.navigationController?.popViewController(animated: true)
            })
        }
    }
    
}
