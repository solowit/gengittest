//
//  SearchRouter.swift
//  GenGitTest
//
//  Created by admin on 10/18/18.
//  Copyright © 2018 VikSolo. All rights reserved.
//

import Foundation
import UIKit

class SearchRouter: SearchRouterProtocol {
    
    static func createSearchModule() -> SearchViewController {
        
        let searchView = SearchViewController(nibName: String(describing: SearchViewController.self), bundle: nil)
        
        let presenter: SearchPresenterProtocol & SearchIntractorOutProtocol = SearchPresenter()
        
        searchView.presenter = presenter
        searchView.presenter?.wireframe = SearchRouter()
        searchView.presenter?.view = searchView
        searchView.presenter?.interactor = SearchInteractor()
        searchView.presenter?.interactor?.presenter = presenter
        
        return searchView
    }
    
    func navigateToLogin(from view: UIViewController) {
        let loginView = LoginViewController(nibName: String(describing: LoginViewController.self), bundle: nil)
        LoginRouter.createLoginModule(loginView: loginView)
        view.navigationController?.pushViewController(loginView, animated: true)
    }
    
    func navigateToDetail(with repoDetail: Repository, for view: UIViewController) {
        let detailRepoView = DetailRepoView(nibName: String(describing: DetailRepoView.self), bundle: nil)
        detailRepoView.title = repoDetail.name
        detailRepoView.repositoryURL = repoDetail.htmlURL
        DeteilRepoRouter.createDetailRepoModule(detailView: detailRepoView, for: repoDetail)
        detailRepoView.modalPresentationStyle = .custom
        view.present(detailRepoView, animated: true, completion: nil)
    }
    
    
}
