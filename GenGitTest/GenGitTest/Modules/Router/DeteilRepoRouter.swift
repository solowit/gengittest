//
//  DetailRouter.swift
//  GenGitTest
//
//  Created by admin on 10/18/18.
//  Copyright © 2018 VikSolo. All rights reserved.
//

import Foundation
import UIKit

class DeteilRepoRouter: DetailRouterProtocol {
    
    static func createDetailRepoModule(detailView: DetailRepoView, for repoDetail: Repository) {
        
        let presenter: DetailPresenterProtocol = DetailPresenter()
        detailView.presenter = presenter
        detailView.presenter?.wireframe = DeteilRepoRouter()
        detailView.presenter?.view = detailView
        detailView.presenter?.interactor = DetailInteractor()
    }
    
    func backToSearch(from view: DetailRepoView) {
        view.dismiss(animated: true, completion: nil)
    }
}
