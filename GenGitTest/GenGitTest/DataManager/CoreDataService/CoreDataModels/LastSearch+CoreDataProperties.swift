//
//  LastSearch+CoreDataProperties.swift
//  GenGitTest
//
//  Created by admin on 10/21/18.
//  Copyright © 2018 VikSolo. All rights reserved.
//
//

import Foundation
import CoreData


extension LastSearch {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<LastSearch> {
        return NSFetchRequest<LastSearch>(entityName: "LastSearch")
    }

    @NSManaged public var name: String?

}
