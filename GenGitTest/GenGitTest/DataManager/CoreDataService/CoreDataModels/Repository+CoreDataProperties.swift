//
//  Repository+CoreDataProperties.swift
//  GenGitTest
//
//  Created by admin on 10/22/18.
//  Copyright © 2018 VikSolo. All rights reserved.
//
//

import Foundation
import CoreData


extension Repository {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Repository> {
        return NSFetchRequest<Repository>(entityName: "Repository")
    }

    @NSManaged public var htmlURL: URL?
    @NSManaged public var id: Int32
    @NSManaged public var name: String?
    @NSManaged public var position: Int32
    @NSManaged public var stars: Int32
    @NSManaged public var viewed: Bool

}
