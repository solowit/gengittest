//
//  CoreDataManager.swift
//  GenGitTest
//
//  Created by admin on 10/18/18.
//  Copyright © 2018 VikSolo. All rights reserved.
//

import Foundation
import CoreData

class CoreDataManager {
    
    static let shared = CoreDataManager()
    
    private init() {
    }
    
    fileprivate let repositoryEntity = String(describing: Repository.self)
    fileprivate let lastSearchEntity = String(describing: LastSearch.self)
    
    func getGitObjects(for name: String = "") -> [Repository]? {
        let managedObjectContext = persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: repositoryEntity)
        do {
            let savedSearchResults = try managedObjectContext.fetch(fetchRequest) as! [Repository]
            return savedSearchResults
        } catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
            return nil
        }
    }
    
    func saveGitObjects(repos:[GitRepo]) {
        let managedObjectContext = persistentContainer.newBackgroundContext()
        for repository in repos {
            if checkIfExist(repositoryId: repository.id) {
                let entity = NSEntityDescription.insertNewObject(forEntityName: repositoryEntity, into: managedObjectContext) as! Repository
                var repositoryName = repository.name
                if repositoryName.count > 30 {
                    let range = repositoryName.index(repositoryName.startIndex, offsetBy: 30)..<repositoryName.endIndex
                    repositoryName.removeSubrange(range)
                }
                entity.name = repositoryName
                entity.id = Int32(repository.id)
                entity.htmlURL = URL(string: repository.htmlURL)
                entity.stars = Int32(repository.starsCount)
            }
        }
        saveContext(context: managedObjectContext)
    }
    
    func removeObject(repository: Repository) {
        let context = persistentContainer.viewContext
        let fetchRequest: NSFetchRequest<Repository> = Repository.fetchRequest()
        fetchRequest.predicate = NSPredicate(format: "id == \(repository.id)")
        let object = try! context.fetch(fetchRequest)
        context.delete(object.first!)
    }
    
    func getLastNameSearched() -> String? {
        let managedObjectContext = persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: lastSearchEntity)
        do {
            let savedSearchResults = try managedObjectContext.fetch(fetchRequest)
            if savedSearchResults.count > 0 {
                let result = savedSearchResults.last as! LastSearch
                return result.name
            }
            return nil
        } catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
            return nil
        }
    }
    
    func changeViewedRepository(for name: String) {
        let context = persistentContainer.newBackgroundContext()
        let fetchRequest = NSFetchRequest<Repository>()
        fetchRequest.entity = NSEntityDescription.entity(forEntityName: repositoryEntity, in: context)
        let result = try? context.fetch(fetchRequest)
        if result != nil, let element = result!.first, !element.viewed {
            result?.first?.viewed = true
            saveContext(context: context)
        }
    }
    
    func saveLastNameSearch(name: String) {
        let managedObjectContext = persistentContainer.viewContext
        let entity = NSEntityDescription.insertNewObject(forEntityName: lastSearchEntity, into: managedObjectContext) as! LastSearch
        entity.name = name
        saveContext(context: managedObjectContext)
    }
    
    func checkIfExist (repositoryId: Int) -> Bool {
        let fetchRequest = NSFetchRequest<Repository>()
        fetchRequest.entity = NSEntityDescription.entity(forEntityName: repositoryEntity, in: persistentContainer.viewContext)
        fetchRequest.predicate = NSPredicate(format: "id == %@", repositoryId.description)
        do {
            let result = try persistentContainer.viewContext.fetch(fetchRequest)
            return result.count == 0
        } catch {
            return false
        }
    }
    
    let persistentContainer: NSPersistentContainer = {
        let container = NSPersistentContainer(name: "CoreDataModel")
        container.loadPersistentStores(completionHandler: { (persistentStoreDescription, error) in
            if error != nil {
                print("Encountered an error while loading persistent container - \(String(describing: error?.localizedDescription))")
            }
        })
        container.viewContext.automaticallyMergesChangesFromParent = true
        container.viewContext.mergePolicy = NSMergePolicy.mergeByPropertyObjectTrump
        return container
    }()
    
    func saveContext(context: NSManagedObjectContext = CoreDataManager.shared.persistentContainer.viewContext) {
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                print("Encountered an error while saving - \(error.localizedDescription)")
            }
        }
    }
    
}
