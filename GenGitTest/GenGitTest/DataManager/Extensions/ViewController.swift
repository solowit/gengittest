//
//  ViewController.swift
//  GenGitTest
//
//  Created by admin on 10/22/18.
//  Copyright © 2018 VikSolo. All rights reserved.
//

import Foundation
import UIKit

extension UIViewController {
    func showAlert(title: String, action: (()->())? = nil) {
        let alertController = UIAlertController(title: title, message: nil, preferredStyle: .alert)
        let action = UIAlertAction(title: "OK", style: .default) { _ in
            action?()
        }
        alertController.addAction(action)
        present(alertController, animated: true, completion: nil)
    }
}
