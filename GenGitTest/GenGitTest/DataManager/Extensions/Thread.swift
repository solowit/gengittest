//
//  Thread.swift
//  GenGitTest
//
//  Created by admin on 10/21/18.
//  Copyright © 2018 VikSolo. All rights reserved.
//

import Foundation

extension Thread {
    static func runInMain(_ operation: @escaping ()->()) {
        if Thread.isMainThread {
            operation()
        } else {
            DispatchQueue.main.async {
                operation()
            }
        }
    }
}
