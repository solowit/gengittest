//
//  OperationQueue.swift
//  GenGitTest
//
//  Created by admin on 10/21/18.
//  Copyright © 2018 VikSolo. All rights reserved.
//

import Foundation

extension OperationQueue {
    func continueAllOperations() {
        self.isSuspended = false
        for operation in operations {
            if let requestOperation = operation as? RequestOperation {
                requestOperation.uppause()
            }
        }
    }
    
    func pauseAllOperations() {
        self.isSuspended = true
        for operation in operations {
            if let requestOperation = operation as? RequestOperation {
                requestOperation.pause()
            }
        }
    }
}
