//
//  RequestOperation.swift
//  GenGitTest
//
//  Created by admin on 10/23/18.
//  Copyright © 2018 VikSolo. All rights reserved.
//

import Foundation

enum RequestState: Equatable {
    case initialized, ready, executing, paused, finished
}

class RequestOperation: Operation {
    
    fileprivate var parameters: Parameters!
    fileprivate var headers: Headers!
    fileprivate var method: RequestMethod!
    fileprivate var succeedHandler: successEmptyComplication!
    fileprivate var failedHandler: complitionFailed!
    
    var currentState: RequestState = .initialized
    
    init(parameters: Parameters, headers: Headers?, for method:RequestMethod, success:@escaping successEmptyComplication, failed:@escaping complitionFailed) {
        super.init()
        self.parameters = parameters
        self.headers = headers
        self.method = method
        self.succeedHandler = success
        self.failedHandler = failed
    }

    override func main() {
        if isReady && isPaused {
            start()
        } else {
            finish()
        }
    }

    func pause() {
        if !isPaused && !isExecuting {
            currentState = .paused
        }
    }
    
    func uppause() {
        if isPaused {
            currentState = .ready
        }
    }
    
    func finish() {
        if isFinished {
            currentState = .finished
        }
    }
    
    func execute() {
        finish()
    }
 
    override var isExecuting: Bool {
        return currentState == .executing
    }
    
    override var isReady: Bool {
        return !(self.isFinished || self.isPaused)
    }
    
    var isPaused: Bool {
        return currentState == .paused
    }
    
    override var isAsynchronous: Bool {
        return true
    }
    
    override func start() {
        super.start()
        if isCancelled {
            finish()
        }
        self.makeRequest(to: baseURL, with: parameters, headers: headers, for: method, success: { response in
            
            guard let items = response["items"] as? [[String: Any]] else {
                self.failedHandler(.empty)
                self.finish()
                return
            }
            let parseOperation = ParseOperation(scope: items, succeed: self.succeedHandler)
            parseOperation.addDependency(self)
            APIManager.instance.operationQueue.addOperation(parseOperation)
            self.finish()
        }
        ) { error in
            self.failedHandler(error)
            self.finish()
        }
    }
    
    fileprivate func makeRequest(to url:String,
                                 with parameters:Parameters,
                                 headers: Headers?,
                                 for method:RequestMethod,
                                 success:@escaping successComplication<[String: Any]>,
                                 failed:@escaping complitionFailed) {
        
        var components = URLComponents(string: url)!
        components.queryItems = parameters.map { (key, value) in
            URLQueryItem(name: key, value: value)
        }
        
        var request = URLRequest(url: components.url!)
        request.httpMethod = method.rawValue
        
        request.allHTTPHeaderFields = headers
        
        let task = APIManager.shared.dataTask(with: request as URLRequest) { data, response, error in
            guard let data = data, let _ : URLResponse = response, error == nil else {
                failed(.other(error: error! as NSError))
                return
            }
            if let json = try? JSONSerialization.jsonObject(with: data, options: []) as! [String: Any] {
                success(json)
            } else {
                failed(.parseError)
            }
        }
        task.resume()
    }
    
}
