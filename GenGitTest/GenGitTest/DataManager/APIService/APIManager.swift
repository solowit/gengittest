//
//  APIManager.swift
//  GenGitTest
//
//  Created by admin on 10/18/18.
//  Copyright © 2018 VikSolo. All rights reserved.
//

import Foundation

enum APIError: Equatable {
    
    case empty, parseError, notLoginned, other(error: NSError)

    static func == (lhs: APIError, rhs: APIError) -> Bool {
        switch (lhs, rhs) {
        case (.empty, .empty): return true
        case (.parseError, .parseError): return true
        case (.notLoginned, .notLoginned): return true
        case (.other(let lhsError) , .other(let rhsError)): return lhsError == rhsError
        default:
            return false
        }
    }
    
    func errorDescription() -> String {
        switch self {
        case .empty: return "Empty list."
        case .parseError: return "Error in parse objects."
        case .notLoginned: return "You're not loggined."
        case .other(error: let error): return error.localizedDescription
        }
    }
}

typealias Parameters = [String: String]
typealias Headers = [String: String]

typealias successComplication<T> = (T) -> ()

typealias successEmptyComplication = () -> Void
typealias complitionFailed = successComplication<APIError>

let baseURL = "https://api.github.com/search/repositories"

let clientId = "288c02cbd89dc0a25023"
let clientSecret = "cde1af15fdf834ffed2b76d7f2a937f729a0ee2e"

let gitLoginURL = "https://github.com/login/oauth/authorize?client_id=\(clientId)"

enum RequestMethod:String {
    case get = "GET"
    case post = "POST"
    case put = "PUT"
}

class APIManager: URLSession {
    
    var operationQueue: OperationQueue = OperationQueue()
    fileprivate var dataTasks: [URLSessionDataTask] = [URLSessionDataTask]()
    
    private var pageSize = 15
    fileprivate var currentPage = 0
    
    public var accessToken: String = ""
    
    static let instance : APIManager = {
        let instance = APIManager()
        return instance
    }()
    
    private override init() {
        super.init()
        operationQueue = OperationQueue()
        operationQueue.maxConcurrentOperationCount = 2
    }
    
    public func stopAllDataTask() {
        operationQueue.pauseAllOperations()
        operationQueue.cancelAllOperations()
    }
    
    public func pauseAllDataTask() {
        operationQueue.pauseAllOperations()
    }
    
    public func resumeAllDataTask() {
        operationQueue.continueAllOperations()
    }
    
    private var authHeaders: Headers {
        return ["Authorization": "Bearer \(accessToken)"]
    }
    
    func getAccessToken(code: String, success: @escaping successEmptyComplication) {
        let urlString = "https://github.com/login/oauth/access_token"
        if let tokenUrl = NSURL(string: urlString) {
            let req = NSMutableURLRequest(url: tokenUrl as URL)
            req.httpMethod = RequestMethod.post.rawValue
            req.addValue("application/json", forHTTPHeaderField: "Content-Type")
            req.addValue("application/json", forHTTPHeaderField: "Accept")
            let params = [
                "client_id" : clientId,
                "client_secret" : clientSecret,
                "code" : code
            ]
            req.httpBody = try? JSONSerialization.data(withJSONObject: params, options: [])
            let task = URLSession.shared.dataTask(with: req as URLRequest) { data, response, error in
                if let data = data {
                    do {
                        if let content = try JSONSerialization.jsonObject(with: data, options: []) as? [String: AnyObject] {
                            if let accessToken = content["access_token"] as? String {
                                APIManager.instance.accessToken = accessToken
                                success()
                            }
                        }
                    } catch {
                        success()
                    }
                }
            }
            task.resume()
        }
    }
    
    func canLoadMore() -> Bool {
        return operationQueue.operations.filter({!$0.isFinished}).count == 0
    }
    
    func getRepository(for name: String, page: Int = 1, success: @escaping successEmptyComplication, failed: @escaping complitionFailed) {
        
        currentPage = page
        
        var headers: Headers? = nil
        if !accessToken.isEmpty {
            headers = authHeaders
        }
        
        let parameters: Parameters = ["q": name,
                                      "sort": "stars",
                                      "page": currentPage.description,
                                      "size" : pageSize.description]
        
        
        let requestOperation = RequestOperation(parameters: parameters, headers: headers, for: .get, success: {
            success()
        }) { error in
            failed(error)
        }
//        requestOperation.completionBlock = successEmptyComplication
        requestOperation.queuePriority = .high
        operationQueue.continueAllOperations()
        operationQueue.addOperation(requestOperation)
    }
}
