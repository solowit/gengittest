//
//  ParseOperation.swift
//  GenGitTest
//
//  Created by admin on 10/23/18.
//  Copyright © 2018 VikSolo. All rights reserved.
//

import UIKit

class ParseOperation: Operation {

    fileprivate var itemsToParse = [[String: Any]]()
    fileprivate var succeedHandler: successEmptyComplication!
    
    init(scope: [[String: Any]], succeed: @escaping successEmptyComplication) {
        super.init()
        succeedHandler = succeed
        itemsToParse = scope
    }
    
    override func start() {
        super.start()
        var searchResult = [GitRepo]()
        for item in itemsToParse {
            let gitRepo:GitRepo? = self.parseData(item: item)
            if gitRepo != nil {
                searchResult.append(gitRepo!)
            } else {
                print("Error parse object \(item)")
                continue
            }
        }
        CoreDataManager.shared.saveGitObjects(repos: searchResult)
        itemsToParse.removeAll()
        succeedHandler()
    }
    
    fileprivate func parseData<T: Decodable>(item:[String:Any]) -> T? {
        var user:T? = nil
        let strJSONData = convertToJSON(dict: item).data(using: .utf8)!
        let decoder = JSONDecoder()
        decoder.dateDecodingStrategy = .iso8601
        do {
            let userData = try decoder.decode(T.self, from: strJSONData)
            user = userData
        } catch {
            return nil
        }
        return user
    }
    
    fileprivate func convertToJSON(dict:[String:Any]) -> String {
        if let theJSONData = try? JSONSerialization.data(withJSONObject: dict, options: []) {
            let theJSONText = String(data: theJSONData, encoding: .utf8)
            return theJSONText!
        } else {
            return ""
        }
    }
}
